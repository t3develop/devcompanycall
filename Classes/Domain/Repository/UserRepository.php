<?php
namespace Devcompany\Devcompanycall\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Users
 */
class UserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	protected $defaultOrderings = array (
		'rate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
		'lastlogin' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);	

    /**
     * Get a random object
     * @return Tx_Extbase_Persistence_QueryResultInterface|array
     */
    public function findRandom() {
        $rows = $this->createQuery()->execute()->count();
        $row_number = mt_rand(0, max(0, ($rows - 1)));
        return $this->createQuery()
        ->setOffset($row_number)
        ->setLimit(5)
        ->setOrderings(array('crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING))
        ->execute();
    }   
		
	/**
	 *
	 * @param $searchFields
	 * @param $searchQuery
	 * @return void
	 */
	public function searchFor($searchFields, $searchQuery) {
		$query = $this->createQuery();
		$constraint = array();
		foreach ($searchFields as $field) {
			$constraint[] = $query->like($field, '%' . $searchQuery . '%');
		}
		$query = $query->matching(
				$query->logicalOr(
						$constraint
				)
		);
		$query->setLimit(60);
	
		$query->setOrderings(array('lastlogin' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
		return $query->execute();
		//$GLOBALS['TYPO3_DB']->debugOutput = true;
	}			 

	public function setRateValue($user) {
		$userUid = $user->getUid();
		$rateTable = 'fe_users';
		$rateField = 'rate';
		$currentRate = $user->getRate();
		$newRate = $currentRate + '10';
		$rateUid = $userUid;	
		$queryResult = $GLOBALS['TYPO3_DB']->exec_UPDATEquery ($rateTable,'uid ='.$rateUid, array($rateField => $newRate)) ;
		
		//$queryResult = $GLOBALS['TYPO3_DB']->exec_UPDATEquery ("UPDATE fe_users SET point=point+10 WHERE uid=$userUid") ;
		
		//$query = $this->createQuery();		
		//$sql = 'UPDATE '.$rateTable.' SET '.$rateField.'='.$currentRate.' WHERE uid='.$rateUid;
		//$query->getQuerySettings()->setReturnRawQueryResult(TRUE);		
		//$query->statement( $sql );
		//return $query->execute();		

		//$query->statement('UPDATE fe_users SET point=point+10 WHERE uid="$userUid" ');
		//$result = $query->execute();	
   } 
   
	public function deleteRateValue($user) {
		$userUid = $user->getUid();
		$rateTable = 'fe_users';
		$rateField = 'rate';
		$currentRate = $user->getRate();
		$newRate = $currentRate - '10';
		$rateUid = $userUid;	
		$queryResult = $GLOBALS['TYPO3_DB']->exec_UPDATEquery ($rateTable,'uid ='.$rateUid, array($rateField => $newRate)) ;	
   } 
      	
}