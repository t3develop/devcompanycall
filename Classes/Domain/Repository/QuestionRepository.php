<?php
namespace Devcompany\Devcompanycall\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Questions
 */
class QuestionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	protected $defaultOrderings = array (
		'create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);
	
	
	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Question $question
	 * @return void
	 */
	public function findQuestionsByUser($user, $limit) {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd(
					$query->equals('user', $user)
				)
		)
		->setLimit($limit)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();
	}	
	
	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Category $category
	 * @return void
	 */
	public function findAll() {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd (
					$query->equals('private', FALSE)					
				)
		)
		->setLimit(100)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();	
	}	

	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Category $category
	 * @return void
	 */
	public function findNonAnswer() {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd(
					$query->equals('answer', FALSE),
					$query->equals('private', FALSE)					
				)
		)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();
	}
	

	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Category $category
	 * @return void
	 */
	public function findByCategory($categories, $limit) {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd(
					$query->equals('private', FALSE),
					//$query->in('categories.uid',explode(',',$settings['flexform']['categories']))
					$query->in('category.uid',explode(',',$categories))
				)
		)
		->setLimit((int)$limit)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();
	}
	
	public function lastAdded () {
		$query = $this->createQuery();
		$result = $query
			->setLimit(20)
			->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
			->execute();
		return $result->toArray();	
	}	
	
	
	/**
	 *
	 * @param $searchFields
	 * @param $searchQuery
	 * @return void
	 */	
	public function searchFor($searchFields, $searchQuery) {
		$query = $this->createQuery();
		$constraint = array();
		foreach ($searchFields as $field) {
			$constraint[] = $query->like($field, '%' . $searchQuery . '%');
		}
		$query = $query->matching(
				$query->logicalOr(
						$constraint
				)
		);
		$query->setLimit(60);
	
		$query->setOrderings(array(
			//'tariff' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
			));
		return $query->execute();
		//$GLOBALS['TYPO3_DB']->debugOutput = true;
	}	
	
	public function setCountValue($question) {
		$questionUid = $question->getUid();
		$rateTable = 'tx_devcompanycall_domain_model_question';
		$rateField = 'count';
		$currentRate = $question->getCount();
		$newRate = $currentRate + '1';
		$rateUid = $questionUid;	
		$queryResult = $GLOBALS['TYPO3_DB']->exec_UPDATEquery ($rateTable,'uid ='.$rateUid, array($rateField => $newRate)) ;	
   } 		
	
}