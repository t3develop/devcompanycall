<?php
namespace Devcompany\Devcompanycall\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Answers
 */
class AnswerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	protected $defaultOrderings = array (
		'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);

	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Question $question
	 * @return void
	 */
	public function findByQuestion($question) {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd(
					$query->equals('question', $question)
				)
		)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();
	}
	
	
	/**
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Answer $answer
	 * @return void
	 */
	public function findAnswersByUser($user, $limit) {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalAnd(
					$query->equals('user', $user)
				)
		)
		->setLimit($limit)
		->setOrderings(array('create_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING))
		->execute();
	}		
	
}