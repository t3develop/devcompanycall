<?php
namespace Devcompany\Devcompanycall\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * User
 */
class User extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * username
	 *
	 * @var string
	 */
	protected $username = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * firstName
	 *
	 * @var string
	 */
	protected $firstName = '';

	/**
	 * lastName
	 *
	 * @var string
	 */
	protected $lastName = '';

	/**
	 * lastlogin
	 *
	 * @var \DateTime
	 */
	protected $lastlogin = '';

	/**
	 * isOnline
	 *
	 * @var \DateTime
	 */
	protected $isOnline = '';
	
	/**
 	* crdate
 	* @var string
 	*/
	protected $crdate;

	/**
	 * image
	 *
	 * @var string
	 */
	protected $image = '';

	/**
	 * question
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question>
	 * @cascade remove
	 */
	protected $question = NULL;

	/**
	 * message
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Messsage>
	 * @cascade remove
	 */
	protected $message = NULL;
	
	/**
	 * comments
	 *
	 * @var string
	 */
	protected $comments = '';
	
	/**
	 * dateOfBirth
	 *
	 * @var string
	 */
	protected $dateOfBirth = NULL;	

	/**
	 * point
	 *
	 * @var string
	 */
	protected $point = 0;	
	
	/**
	 * rate
	 *
	 * @var integer
	 */
	protected $rate = 0;
	
	/**
	 * gender
	 *
	 * @var integer
	 */
	protected $gender = 0;	
	
	/**
	 * twitterId
	 *
	 * @var \string
	 */
	protected $twitterId;

	/**
	 * skypeId
	 *
	 * @var \string
	 */
	protected $skypeId;
	
	/**
	 * vkId
	 *
	 * @var \string
	 */
	protected $vkId;
	
	/**
	 * facebookId
	 *
	 * @var \string
	 */
	protected $facebookId;	
	
	/**
	 * category
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Category>
	 */
	protected $category = NULL;	
		

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->question = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->message = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the username
	 *
	 * @return string $username
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * Sets the username
	 *
	 * @param string $username
	 * @return void
	 */
	public function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the firstName
	 *
	 * @return string $firstName
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Sets the firstName
	 *
	 * @param string $firstName
	 * @return void
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * Returns the lastName
	 *
	 * @return string $lastName
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * Sets the lastName
	 *
	 * @param string $lastName
	 * @return void
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * Returns the lastlogin
	 *
	 * @return \DateTime $lastlogin
	 */
	public function getLastlogin() {
		return $this->lastlogin;
	}

	/**
	 * Sets the lastlogin
	 *
	 * @param \DateTime $lastlogin
	 * @return void
	 */
	public function setLastlogin($lastlogin) {
		$this->lastlogin = $lastlogin;
	}

	/**
	 * Returns the isOnline
	 *
	 * @return \DateTime $isOnline
	 */
	public function getIsOnline() {
		return $this->isOnline;
	}

	/**
	 * Sets the isOnline
	 *
	 * @param \DateTime $isOnline
	 * @return void
	 */
	public function setIsOnline($isOnline) {
		$this->isOnline = $isOnline;
	}

	/**
	 * Returns the image
	 *
	 * @return string $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param string $image
	 * @return void
	 */
	/* 
	public function setImage($image) {
		$this->image = $image;
	}
	*/

	/**
	 * Adds a Question
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function addQuestion(\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$this->question->attach($question);
	}

	/**
	 * Removes a Question
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $questionToRemove The Question to be removed
	 * @return void
	 */
	public function removeQuestion(\Devcompany\Devcompanycall\Domain\Model\Question $questionToRemove) {
		$this->question->detach($questionToRemove);
	}

	/**
	 * Returns the question
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question> $question
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Sets the question
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question> $question
	 * @return void
	 */
	public function setQuestion(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $question) {
		$this->question = $question;
	}

	/**
	 * Adds a Messsage
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $message
	 * @return void
	 */
	public function addMessage(\Devcompany\Devcompanycall\Domain\Model\Messsage $message) {
		$this->message->attach($message);
	}

	/**
	 * Removes a Messsage
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messageToRemove The Messsage to be removed
	 * @return void
	 */
	public function removeMessage(\Devcompany\Devcompanycall\Domain\Model\Messsage $messageToRemove) {
		$this->message->detach($messageToRemove);
	}

	/**
	 * Returns the message
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Messsage> $message
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Sets the message
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Messsage> $message
	 * @return void
	 */
	public function setMessage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $message) {
		$this->message = $message;
	}
	
	/**
	 * Returns the comments
	 *
	 * @return string $comments
	 */
	public function getComments() {
		return $this->comments;
	}

	/**
	 * Sets the comments
	 *
	 * @param string $comments
	 * @return void
	 */
	public function setComments($comments) {
		$this->comments = $comments;
	}	

 
	/**
 	* @param \DateTime $crdate
 	* @return void
 	*/
	public function setCrdate($crdate) {
  	$this->crdate = $crdate;
	}
 
	/**
 	* @return \DateTime
 	*/
	public function getCrdate() {
  	return $this->crdate;
	}

	/**
	 * Returns the dateOfBirth
	 *
	 * @return \DateTime $dateOfBirth
	 */
	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}

	/**
	 * Sets the dateOfBirth
	 *
	 * @param \DateTime $dateOfBirth
	 * @return void
	 */
	public function setDateOfBirth($dateOfBirth) {
		$this->dateOfBirth = $dateOfBirth;
	}
	
	/**
	 * Counts age from date of birth
	 * @return integer
	 */
	/*
	public function getAge() {
		$age = date('Y')-$this->dateOfBirth->format('Y');
		if (date('m') < $this->dateOfBirth->format('m')) {
			$age--;
		} elseif (date('m') == $this->dateOfBirth->format('m') && date('d') < $this->dateOfBirth->format('d')) {
			$age--;
		}
		return $age;
	}	
	*/	
	
	/**
	 * Returns the point
	 *
	 * @return string point
	 */
	public function getPoint() {
		return $this->point;
	}

	/**
	 * Sets the point
	 *
	 * @param string $point
	 * @return void
	 */
	public function setPoint($point) {
		$this->point = $point;
	}	

	/**
	 * Returns the rate
	 *
	 * @return integer $rate
	 */
	public function getRate() {
		return $this->rate;
	}

	/**
	 * Sets the rate
	 *
	 * @param integer $rate
	 * @return void
	 */
	public function setRate($rate) {
		$this->rate = $rate;
	}

	/**
	 * Returns the gender
	 *
	 * @return integer $gender
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * Sets the gender
	 *
	 * @param integer $gender
	 * @return void
	 */
	public function setGender($gender) {
		$this->gender = $gender;
	}
	
	
	/**
	 * Returns the twitterId
	 *
	 * @return \string $twitterId
	 */
	public function getTwitterId() {
		return $this->twitterId;
	}

	/**
	 * Sets the twitterId
	 *
	 * @param \string $twitterId
	 * @return void
	 */
	public function setTwitterId($twitterId) {
		$this->twitterId = $twitterId;
	}
	
	/**
	 * Returns the skypeId
	 *
	 * @return \string $skypeId
	 */
	public function getSkypeId() {
		return $this->skypeId;
	}

	/**
	 * Sets the skypeId
	 *
	 * @param \string $skypeId
	 * @return void
	 */
	public function setSkypeId($skypeId) {
		$this->skypeId = $skypeId;
	}
	
	/**
	 * Returns the vkId
	 *
	 * @return \string $vkId
	 */
	public function getVkId() {
		return $this->vkId;
	}

	/**
	 * Sets the vkId
	 *
	 * @param \string $vkId
	 * @return void
	 */
	public function setVkId($vkId) {
		$this->vkId = $vkId;
	}
	
	/**
	 * Returns the facebookId
	 *
	 * @return \string $facebookId
	 */
	public function getFacebookId() {
		return $this->facebookId;
	}

	/**
	 * Sets the facebookId
	 *
	 * @param \string $facebookId
	 * @return void
	 */
	public function setFacebookId($facebookId) {
		$this->facebookId = $facebookId;
	}
	
	/**
	 * Adds a Category
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\Devcompany\Devcompanycall\Domain\Model\Category $category) {
		$this->category->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(\Devcompany\Devcompanycall\Domain\Model\Category $categoryToRemove) {
		$this->category->detach($categoryToRemove);
	}
	
	/**
	 * Returns the category
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Category> $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Sets the category
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Category> $category
	 * @return void
	 */
	public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category) {
		$this->category = $category;
	}						

}