<?php
namespace Devcompany\Devcompanycall\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
 use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Question
 */
class Question extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var \int
	 */
	protected $uid;		

	/**
	 * title
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $title = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * createDate
	 *
	 * @var \DateTime
	 */
	protected $createDate = NULL;

	/**
	 * category
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\Category
	 */
	protected $category = NULL;

	/**
	 * answer
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Answer>
	 * @cascade remove
	 */
	protected $answer = NULL;

	/**
	 * Image
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image;
	
	/**
	 * Image
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 */
	protected $imageCollection;	
	
	/**
	 * user
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $user = NULL;
	
	/**
	 * sender
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $sender = NULL;	
	
	/**
	 * recipient
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $recipient = NULL;	
	
	/**
	 * videoId
	 *
	 * @var string
	 */
	protected $videoId = '';	
	
	/**
	 * notice
	 *
	 * @var boolean
	 */
	protected $notice = FALSE;	
	
	/**
	 * count
	 *
	 * @var integer
	 */
	protected $count = 0;	
	
	/**
	 * private
	 *
	 * @var boolean
	 */
	protected $private = FALSE;	
		

	/**
	 * __construct
	 */
	public function __construct() {
		$this->createDate = new \DateTime();
		$this->imageCollection = new ObjectStorage();
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->answer = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}
	
	/**
	 * Return the uid
	 *
	 * @return \int $uid
	 */
	public function getUid() {
		return $this->uid;
	}	

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the createDate
	 *
	 * @return \DateTime $createDate
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 * Sets the createDate
	 *
	 * @param \DateTime $createDate
	 * @return void
	 */
	public function setCreateDate(\DateTime $createDate) {
		$this->createDate = $createDate;
	}

	/**
	 * Returns the category
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\Category $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Sets the category
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Category $category
	 * @return void
	 */
	public function setCategory(\Devcompany\Devcompanycall\Domain\Model\Category $category) {
		$this->category = $category;
	}

	/**
	 * Adds a Answer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function addAnswer(\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		$this->answer->attach($answer);
	}

	/**
	 * Removes a Answer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answerToRemove The Answer to be removed
	 * @return void
	 */
	public function removeAnswer(\Devcompany\Devcompanycall\Domain\Model\Answer $answerToRemove) {
		$this->answer->detach($answerToRemove);
	}

	/**
	 * Returns the answer
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Answer> $answer
	 */
	public function getAnswer() {
		return $this->answer;
	}

	/**
	 * Sets the answer
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Answer> $answer
	 * @return void
	 */
	public function setAnswer(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $answer) {
		$this->answer = $answer;
	}


	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}


	/**
	 * Returns the user
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $user
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Sets the user
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function setUser(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->user = $user;
	}

	/**
	 * Returns the number of comments
	 *
	 * @return integer The number of comments
	 */
	public function getNumberOfAnswers() {
		return count($this->answer);
	}

	/**
	 * Returns the videoId
	 *
	 * @return string $videoId
	 */
	public function getVideoId() {
		return $this->videoId;
	}

	/**
	 * Sets the videoId
	 *
	 * @param string $videoId
	 * @return void
	 */
	public function setVideoId($videoId) {
		$this->videoId = $videoId;
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $imageCollection
	 */
	public function setImageCollection($imageCollection) {
		$this->imageCollection = $imageCollection;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImageCollection() {
		return $this->imageCollection;
	}	
	

	/**
	 * Returns the notice
	 *
	 * @return boolean $notice
	 */
	public function getNotice() {
		return $this->notice;
	}

	/**
	 * Sets the notice
	 *
	 * @param boolean $notice
	 * @return void
	 */
	public function setNotice($notice) {
		$this->notice = $notice;
	}

	/**
	 * Returns the boolean state of notice
	 *
	 * @return boolean
	 */
	public function isNotice() {
		return $this->notice;
	}

	/**
	 * Returns the count
	 *
	 * @return integer $count
	 */
	public function getCount() {
		return $this->count;
	}

	/**
	 * Sets the count
	 *
	 * @param integer $count
	 * @return void
	 */
	public function setCount($count) {
		$this->count = $count;
	}
	
	/**
	 * Returns the sender
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $sender
	 */
	public function getSender() {
		return $this->sender;
	}

	/**
	 * Sets the sender
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $sender
	 * @return void
	 */
	public function setSender(\Devcompany\Devcompanycall\Domain\Model\User $sender) {
		$this->sender = $sender;
	}
	
	/**
	 * Returns the recipient
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $recipient
	 */
	public function getRecipient() {
		return $this->recipient;
	}

	/**
	 * Sets the recipient
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $recipient
	 * @return void
	 */
	public function setRecipient(\Devcompany\Devcompanycall\Domain\Model\User $recipient) {
		$this->recipient = $recipient;
	}	
	
	/**
	 * Returns the boolean state of private
	 *
	 * @return boolean
	 */
	public function isPrivate() {
		return $this->private;
	}

	/**
	 * Returns the private
	 *
	 * @return boolean private
	 */
	public function getPrivate() {
		return $this->private;
	}

	/**
	 * Sets the private
	 *
	 * @param boolean $private
	 * @return boolean private
	 */
	public function setPrivate($private) {
		$this->private = $private;
	}		

}