<?php
namespace Devcompany\Devcompanycall\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Category
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * parentcategory
	 *
	 * @var string
	 */
	protected $parentcategory = '';

	/**
	 * question
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question>
	 * @cascade remove
	 */
	protected $question = NULL;
	
	/**
	 * user
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\User>
	 */
	protected $user = NULL;	
	
	/**
	 * link
	 *
	 * @var \string
	 */
	protected $link;	

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->question = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->user = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the parentcategory
	 *
	 * @return string $parentcategory
	 */
	public function getParentcategory() {
		return $this->parentcategory;
	}

	/**
	 * Sets the parentcategory
	 *
	 * @param string $parentcategory
	 * @return void
	 */
	public function setParentcategory($parentcategory) {
		$this->parentcategory = $parentcategory;
	}

	/**
	 * Adds a Question
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function addQuestion(\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$this->question->attach($question);
	}

	/**
	 * Removes a Question
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $questionToRemove The Question to be removed
	 * @return void
	 */
	public function removeQuestion(\Devcompany\Devcompanycall\Domain\Model\Question $questionToRemove) {
		$this->question->detach($questionToRemove);
	}

	/**
	 * Returns the question
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question> $question
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Sets the question
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\Question> $question
	 * @return void
	 */
	public function setQuestion(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $question) {
		$this->question = $question;
	}

	/**
	 * Returns the number of Questions
	 *
	 * @return integer The number of Questions
	 */
	public function getNumberOfQuestions() {
		return count($this->question);
	}

	/**
	 * Returns the link
	 *
	 * @return \string $link
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * Sets the link
	 *
	 * @param \string $link
	 * @return void
	 */
	public function setLink($link) {
		$this->link = $link;
	}
	
	/**
	 * Adds a User
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function addUser(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->user->attach($user);
	}

	/**
	 * Removes a User
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $userToRemove The User to be removed
	 * @return void
	 */
	public function removeUser(\Devcompany\Devcompanycall\Domain\Model\User $userToRemove) {
		$this->user->detach($userToRemove);
	}

	/**
	 * Returns the user
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\User> $user
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * Sets the user
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Devcompany\Devcompanycall\Domain\Model\User> $user
	 * @return void
	 */
	public function setUser(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $user) {
		$this->user = $user;
	}		

}