<?php
namespace Devcompany\Devcompanycall\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Wall
 */
class Wall extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * subject
	 *
	 * @var string
	 */
	protected $subject = '';

	/**
	 * message
	 *
	 * @var string
	 */
	protected $message = '';

	/**
	 * createDate
	 *
	 * @var \DateTime
	 */
	protected $createDate = NULL;

	/**
	 * image
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\FileReference
	 */
	protected $image = NULL;

	/**
	 * sender
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $sender = NULL;

	/**
	 * recipient
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $recipient = NULL;

	/**
	 * Returns the subject
	 *
	 * @return string $subject
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * Sets the subject
	 *
	 * @param string $subject
	 * @return void
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * Returns the message
	 *
	 * @return string $message
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Sets the message
	 *
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * Returns the createDate
	 *
	 * @return \DateTime $createDate
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 * Sets the createDate
	 *
	 * @param \DateTime $createDate
	 * @return void
	 */
	public function setCreateDate(\DateTime $createDate) {
		$this->createDate = $createDate;
	}

	/**
	 * Returns the image
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\FileReference $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\FileReference $image
	 * @return void
	 */
	public function setImage(\Devcompany\Devcompanycall\Domain\Model\FileReference $image) {
		$this->image = $image;
	}

	/**
	 * Returns the sender
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $sender
	 */
	public function getSender() {
		return $this->sender;
	}

	/**
	 * Sets the sender
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $sender
	 * @return void
	 */
	public function setSender(\Devcompany\Devcompanycall\Domain\Model\User $sender) {
		$this->sender = $sender;
	}

	/**
	 * Returns the recipient
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $recipient
	 */
	public function getRecipient() {
		return $this->recipient;
	}

	/**
	 * Sets the recipient
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $recipient
	 * @return void
	 */
	public function setRecipient(\Devcompany\Devcompanycall\Domain\Model\User $recipient) {
		$this->recipient = $recipient;
	}

}