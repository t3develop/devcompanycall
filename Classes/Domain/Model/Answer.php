<?php
namespace Devcompany\Devcompanycall\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

 use TYPO3\CMS\Extbase\Persistence\ObjectStorage; 
 
/**
 * Answer
 */
class Answer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * answer
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $answer = '';

	/**
	 * createDate
	 *
	 * @var \DateTime
	 */
	protected $createDate = NULL;
	
	/**
	 * createDate
	 *
	 * @var \DateTime
	 */
	protected $crdate = NULL;	

	/**
	 * user
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $user = NULL;

	/**
	 * question
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\Question
	 */
	protected $question = NULL;

	/**
	 * Image
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image;
	
	/**
	 * Image
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 */
	protected $imageCollection;	
	
	/**
	 * videoId
	 *
	 * @var string
	 */
	protected $videoId = '';	
	
	
	/**
	 * __construct
	 */
	public function __construct() {
		$this->createDate = new \DateTime();
		$this->imageCollection = new ObjectStorage();
	}	

	/**
	 * Returns the answer
	 *
	 * @return string $answer
	 */
	public function getAnswer() {
		return $this->answer;
	}

	/**
	 * Sets the answer
	 *
	 * @param string $answer
	 * @return void
	 */
	public function setAnswer($answer) {
		$this->answer = $answer;
	}

	/**
	 * Returns the createDate
	 *
	 * @return \DateTime $createDate
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 * Sets the createDate
	 *
	 * @param \DateTime $createDate
	 * @return void
	 */
	public function setCreateDate(\DateTime $createDate) {
		$this->createDate = $createDate;
	}
	
	/**
	 * Returns the crdate
	 *
	 * @return \DateTime $crdate
	 */
	public function getCrdate() {
		return $this->crdate;
	}	

	/**
	 * Sets the crdate
	 *
	 * @param \DateTime $crdate
	 * @return void
	 */
	public function setCrdate(\DateTime $crdate) {
		$this->crdate = $crdate;
	}

	/**
	 * Returns the user
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $user
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Sets the user
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function setUser(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->user = $user;
	}

	/**
	 * Returns the question
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\Question $question
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Sets the question
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function setQuestion(\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$this->question = $question;
	}

	/**
	 * Returns the image
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Sets the image
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function setImage($image) {
		$this->image = $image;
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $imageCollection
	 */
	public function setImageCollection($imageCollection) {
		$this->imageCollection = $imageCollection;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImageCollection() {
		return $this->imageCollection;
	}	
	
	/**
	 * Returns the videoId
	 *
	 * @return string $videoId
	 */
	public function getVideoId() {
		return $this->videoId;
	}

	/**
	 * Sets the videoId
	 *
	 * @param string $videoId
	 * @return void
	 */
	public function setVideoId($videoId) {
		$this->videoId = $videoId;
	}		

}