<?php
namespace Devcompany\Devcompanycall\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Reply
 */
class Reply extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * reply
	 *
	 * @var string
	 */
	protected $reply = '';
	
	/**
	 * createDate
	 *
	 * @var \DateTime
	 */
	protected $createDate = NULL;	

	/**
	 * user
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $user = NULL;
	
	/**
	 * messsage
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Model\Messsage
	 */
	protected $messsage = NULL;	
	
	
	/**
	 * __construct
	 */
	public function __construct() {
		$this->createDate = new \DateTime();
	}	
	

	/**
	 * Returns the reply
	 *
	 * @return string $reply
	 */
	public function getReply() {
		return $this->reply;
	}

	/**
	 * Sets the reply
	 *
	 * @param string $reply
	 * @return void
	 */
	public function setReply($reply) {
		$this->reply = $reply;
	}

	/**
	 * Returns the user
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\User $user
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Sets the user
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function setUser(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->user = $user;
	}
	
	/**
	 * Returns the createDate
	 *
	 * @return \DateTime $createDate
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 * Sets the createDate
	 *
	 * @param \DateTime $createDate
	 * @return void
	 */
	public function setCreateDate(\DateTime $createDate) {
		$this->createDate = $createDate;
	}	
	
	/**
	 * Returns the messsage
	 *
	 * @return \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 */
	public function getMesssage() {
		return $this->messsage;
	}

	/**
	 * Sets the messsage
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @return void
	 */
	public function setMesssage(\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage) {
		$this->messsage = $messsage;
	}	
	

}