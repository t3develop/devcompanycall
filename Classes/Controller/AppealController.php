<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * AppealController
 */
class AppealController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;	
	
	/**
	 * accessControll
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;	
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;			

	/**
	 * appealRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\AppealRepository
	 * @inject
	 */
	protected $appealRepository = NULL;
	
	
	/**
	 * send email using swiftmailer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $addMessage
	 * @param string $recipientEmail
	 * @param string $recipientCc
	 * @param string $subject
	 * @param string $message
	 * @param string $senderEmail
	 * @param string $senderName
	 * @return void
	 */
	protected function sendMail($senderEmail, $recipientEmail, $subject, $message) {
		$this->view->assign('settings', $this->settings);
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		//$mail->setFrom($senderEmail);
		$mail->setFrom(array($this->settings['senderEmail']));
		$mail->setTo(array($this->settings['adminEmail']));
		$mail->setSubject($this->settings['appealEmailSubject']);
		//$mail->setCc($recipientCc);
		$mail->setBody(htmlspecialchars_decode($message), 'text/html');
		if($this->settings['debugMail'] == 1) {
			$this->debug($message);
		} else {
			$mail->send();
		}
	}	
	

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$appeals = $this->appealRepository->findAll();
		$this->view->assign('appeals', $appeals);
	}

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $appeal
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\Appeal $appeal) {
		$this->view->assign('appeal', $appeal);
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $newAppeal
	 * @ignorevalidation $newAppeal
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\Appeal $newAppeal = NULL) {
		$this->view->assign('newAppeal', $newAppeal);
	}

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $newAppeal
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question,
	\Devcompany\Devcompanycall\Domain\Model\Appeal $newAppeal) {
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
		$this->view->assign('settings', $this->settings);
		$this->view->assign('user', $user);		
		$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$newAppeal->setSender($loggedUser);	
		$newAppeal->setQuestion($question);		
		$this->persistenceManager->persistAll();
		
		$senderUserEmail = $loggedUser->getEmail();	
		$senderUserName = $loggedUser->getFirstName() .' '. $loggedUser->getLastName();	

		$message .= '<h3>Вам пришла жалоба на пользователя с сайта Обращайся.рф</h3>';
		$message .= '<b>Имя пользователя пославшего жалобу: </b>' . $senderUserName;
		$message .= '<br />';
		$message .= '<b>Сообщение пользователя пославшего жалобу: </b>' . htmlspecialchars($newAppeal->getMessage());
		$message .= '<br /><br />';
		$message .= '<b>Ссылка на проблемное сообщение: </b>' . 'http://обращайся.рф/show/question/' . $question->getUid() . '/';		
		$message .= '<br />';
		$message .= '<b>Автор проблемного сообщения: </b>' . $question->getUser()->getFirstName() . ' ' . $question->getUser()->getLastName();
		$message .= '<br />';
		$message .= '<b>Страница автора проблемного сообщения: </b>' . 'http://обращайся.рф/profile/people/user/' .$question->getUser()->getUid() . '/';
		$message .= '<br />';		
		$message .= '<b>Заголовок проблемного сообщения: </b>' . $question->getTitle();
		$message .= '<br />';
		$message .= '<b>Текст проблемного сообщения: </b><br />' . htmlspecialchars($question->getDescription());
		$message .= '<br />';
		
		// send email
		$this->sendMail(
				$senderEmail,
				$this->settings['adminEmail'],
				$this->settings['appealEmailSubject'],
				$message
				//$newMesssage->getMessage()
		);	
		
		$this->addFlashMessage('Ваша жалоба отправлена, в ближайшее время мы рассмотрим ее. Благодарим вас.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
		$this->appealRepository->add($newAppeal);
		$this->redirect('last','Question',null,array(), '1');		
	}

	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $appeal
	 * @ignorevalidation $appeal
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\Appeal $appeal) {
		$this->view->assign('appeal', $appeal);
	}

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $appeal
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\Appeal $appeal) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->appealRepository->update($appeal);
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Appeal $appeal
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\Appeal $appeal) {
		$this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->appealRepository->remove($appeal);
		$this->redirect('list');
	}

}