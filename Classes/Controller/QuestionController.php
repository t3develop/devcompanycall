<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use Devcompany\Devcompanycall\Property\TypeConverter\UploadedFileReferenceConverter;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Extbase\Property\Exception\TargetNotFoundException;

/**
 * QuestionController
 */
class QuestionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;
	
	/**
	 * categoryRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;		
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;			

	/**
	 * questionRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;
	
	/**
	 * answerRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\AnswerRepository
	 * @inject
	 */
	protected $answerRepository = NULL;	
	
	/**
	 * Access Control
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;	
	
	/**
	 * YouTubeUtility
	 *
	 * @var \Devcompany\Devcompanycall\Utility\YouTubeUtility
	 * @inject
	 */
	protected $youTubeUtility;	
		
	
	/**
	* @return void
	*/
	/*
	protected function initializeCreateAction() {
 		$propertyMappingConfiguration = $this->arguments['newQuestion']->getPropertyMappingConfiguration();
 		$propertyMappingConfiguration->allowAllProperties();
 		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	
    if (isset($this->arguments['newQuestion'])) {
		$this->arguments['newQuestion']
		->getPropertyMappingConfiguration()
        ->forProperty('createDate')
        ->setTypeConverterOption('TYPO3\\CMS\\Extbase\\Property\\TypeConverter\\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'd.m.Y');        }
	}
	*/	
	
	
	/**
	 * send email using swiftmailer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $addMessage
	 * @param string $recipientEmail
	 * @param string $recipientCc
	 * @param string $subject
	 * @param string $message
	 * @param string $senderEmail
	 * @param string $senderName
	 * @return void
	 */
	protected function sendMail($senderEmail, $recipientEmail, $subject, $message) {
		$this->view->assign('settings', $this->settings);
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		//$mail->setFrom($senderEmail);
		$mail->setFrom(array($this->settings['senderEmail']));
		$mail->setTo($recipientEmail);
		$mail->setSubject($this->settings['userEmailSubject']);
		//$mail->setCc($recipientCc);
		$mail->setBody(htmlspecialchars_decode($message), 'text/html');
		if($this->settings['debugMail'] == 1) {
			$this->debug($message);
		} else {
			$mail->send();
		}
	}	

	/**
	 * Initializes the current action
	 *
	 * @return void
	 */
	public function initializeAction() {
		if (isset($this->settings['format'])) {
			$this->request->setFormat($this->settings['format']);
		}
	}	
	
	
	
// Cache
    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\AbstractFrontend
     */
    //protected $cacheInstance;

    /**
     * Constructor
     */
    //public function __construct() {
    //    $this->initializeCache();
    //}
    /**
     * Initialize cache instance to be ready to use
     *
     * @return void
     */
    /*protected function initializeCache() {
        \TYPO3\CMS\Core\Cache\Cache::initializeCachingFramework();
        try {
          $this->cacheInstance = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('devcompanycall_cache');
				} catch (\TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException $e) {
            $this->cacheInstance = $GLOBALS['typo3CacheFactory']->create(
                'devcompanycall_cache',
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['frontend'],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['backend'],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['options']
            );
        }
    }		
		*/
	
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$categories = $this->settings['categories'];
		$limit = $this->settings['limit'];
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('user', $user);			
		//$questions = $this->questionRepository->findAll();
		
		// Set default limit
		if ($limit == NULL) {
			$limit = 9999;	
		}
		$questions = $this->questionRepository->findByCategory($categories, $limit);
		$this->view->assign('questions', $questions);
	}
	
	/**
	 * action non answer list
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function nonAnswerListAction() {
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('user', $user);			
		$questions = $this->questionRepository->findNonAnswer();
		$this->view->assign('questions', $questions);
	}		
	
	/**
	 * action last
	 *
	 * @return void
	 */
	public function lastAction() {
		$categories = $this->settings['categories'];
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('user', $user);			
				
		//$questions = $this->questionRepository->findLast($categories);
		//$this->view->assign('questions', $questions);
		//$this->view->assign('questions', $this->questionRepository->lastAdded());
		$this->view->assign('questions', $this->questionRepository->findAll());
	}	

	/**
	 * action show
	 * 
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$newAnswer = new \Devcompany\Devcompanycall\Domain\Model\Answer();
		$this->view->assign('newAnswer', $newAnswer);				
		$this->view->assign('settings', $this->settings);
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));
		$this->view->assign('user', $user);			
		$this->view->assign('question', $question);		
		$this->view->assign('answers', $this->answerRepository->findByQuestion($question));			
		//$this->view->assign('answerVideo', $this->answerRepository->findByQuestion($question));	
	
		$videoUrl = $question->getVideoId();	
		$videoId = $this->youTubeUtility->getVideoId($videoUrl);
		$this->view->assign('videoId', $videoId);		
		$this->questionRepository->setCountValue($question);
		$this->persistenceManager->persistAll();			
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $newQuestion
	 * @throws TargetNotFoundException
	 * @ignorevalidation $newQuestion
	 * @return void
	 */
	 
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Question $newQuestion = NULL) {		
			if($this->accessControllService->isAccessAllowed($user)) {				
				$newQuestion = new \Devcompany\Devcompanycall\Domain\Model\Question();
				//$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
				$this->view->assign('user', $user);			
				$this->view->assign('category', $this->categoryRepository->findAll());
				$this->view->assign('newQuestion', $newQuestion);				
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last','Question',null,array(), '1');	
				//throw new TargetNotFoundException('You must be login!', 1399312431);
			}
	}
		
	public function newPrivateAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Question $newQuestion = NULL) {
			$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));
					
			if($this->accessControllService->isAccessAllowed($loggedUser)) {				
				$newQuestion = new \Devcompany\Devcompanycall\Domain\Model\Question();
				//$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
				$this->view->assign('user', $user);			
				$this->view->assign('category', $this->categoryRepository->findAll());
				$this->view->assign('newQuestion', $newQuestion);				
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last','Question',null,array(), '1');	
				//throw new TargetNotFoundException('You must be login!', 1399312431);
			}
	}		
		

	  /**
	 	* Set TypeConverter option for image upload
	 	*/
		public function initializeCreateAction() {
			$this->setTypeConverterConfigurationForImageUpload('newQuestion');
		}	
				

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $newQuestion
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Question $newQuestion) {	
			if($this->accessControllService->isAccessAllowed($user)) {
				$this->view->assign('settings', $this->settings);
				//$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));
				$this->view->assign('user', $user);			
				$newQuestion->setUser($user);				
				$this->questionRepository->add($newQuestion);
				$this->persistenceManager->persistAll();

				//$questionUid = $$newQuestion->getUid();

				$this->redirect('last','Question',null,array(), '1');	
				//$this->redirect('show','Question',null,array(), '$questionUid');	
				$this->addFlashMessage(
					'Вы задали вопрос на сайте Обращайся.рф', 
					'', 
					\TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
					FALSE);	
			
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('flash', 'Question');				
			}	
	}


	/**
	 * action create private
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $newQuestion
	 * @return void
	 */
	public function createPrivateAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Question $newQuestion) {
			$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));			
			if($this->accessControllService->isAccessAllowed($loggedUser)) {
				$this->view->assign('settings', $this->settings);
				$this->view->assign('user', $user);		
				$this->view->assign('category', $this->categoryRepository->findAll());					
				
				$newQuestion->setUser($loggedUser);				
				$newQuestion->setRecipient($user);	
				$newQuestion->setSender($loggedUser);
				$private = '1';			
				$newQuestion->setPrivate($private);
				$this->persistenceManager->persistAll();
							
				$senderUserEmail = $loggedUser->getEmail();	
				$senderUserName = $loggedUser->getFirstName() .' '. $loggedUser->getLastName();	
	
		// Email
		$message .= '<h3>Вам пришел вопрос от пользователя с сайта Обращайся.рф</h3>';
		$message .= '<b>Имя пользователя: </b>' . $senderUserName;
		$message .= '<br />';
		//$message .= '<b>Email пользователя: </b>' . $senderUserEmail;
		//$message .= '<br />';
		$message .= '<b>Заголовок вопроса: </b>' . htmlspecialchars($newQuestion->getTitle());
		$message .= '<br />';
		$message .= '<b>Описание вопроса: </b>' . htmlspecialchars($newQuestion->getDescription());
		$message .= '<br />';		
		$message .= '<b>Вы можете отслеживать ваши личные сообщения на странице: </b>';
		$message .= '<a href="http://обращайся.рф/profile/messages/">' .'http://обращайся.рф/profile/messages/' .'</a>';
		$message .= '<br />';	
		
		// send email
		$this->sendMail(
				$senderEmail,
				$user->getEmail(),
				$this->settings['userEmailQuestionSubject'],
				$message
				//$newMesssage->getMessage()
		);			
				
				$this->addFlashMessage('Ваше вопрос отправлен', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
				$this->questionRepository->add($newQuestion);

				$this->redirect('last','Question',null,array(), '1');	
				$this->addFlashMessage(
					'Вы задали вопрос на сайте Обращайся.рф', 
					'', 
					\TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
					FALSE);	
			
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('flash', 'Question');				
			}	
	}


	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @ignorevalidation $question
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$numberOfAnswers = $question->getNumberOfAnswers();		
		if ($numberOfAnswers == 0) {
			if($this->accessControllService->isAccessAllowed($user)) {
				$this->view->assign('category', $this->categoryRepository->findAll());
				$this->view->assign('question', $question);	
				$this->view->assign('user', $user);						
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last','Question',null,array(), '1');					
			}	
		} else {
			$this->redirect('last','Question',null,array(), '1');			
		}
	}
	
	/**
	 * Set TypeConverter option for image upload
	 */
	public function initializeUpdateAction() {
		$this->setTypeConverterConfigurationForImageUpload('question');
	}	

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		if($this->accessControllService->isAccessAllowed($user)) {
			$this->view->assign('user', $user);	
			$this->addFlashMessage('Запись обновлена', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
			$this->questionRepository->update($question);
			//$this->redirect('list');	
			$this->redirect('ok', 'Flash');
			//$this->redirect('last','Question','','', '1');			
		} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('error', 'Flash');			
		}		
	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question) {
		$numberOfAnswers = $question->getNumberOfAnswers(); 	
		if ($numberOfAnswers == 0) {
			if($this->accessControllService->isAccessAllowed($user)) {
				$this->addFlashMessage('Ваш вопрос удален', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
				$this->questionRepository->remove($question);
				//$this->redirect('list');	
				$this->redirect('ok', 'Flash');				
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('error', 'Flash');				
			}
			
		} else {
			$this->redirect('last','Question',null,array(), '1');	
		}
	}


	/**
	 * ask buton using in template
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $newQuestion
	 * @ignorevalidation $newQuestion
	 * @return void
	 */	
	public function askButtonAction(\Devcompany\Devcompanycall\Domain\Model\Question $newQuestion = NULL) {		
			$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
			$this->view->assign('user', $user);			
			$this->view->assign('newQuestion', $newQuestion);				
	}	
	
	
	/**
	 * Search item
	 * 
	 * @param string $searchFields
	 * @param string $searchPhrase
	 * @dontvalidate $searchPhrase
	 * @ignorevalidation $searchPhrase
	 * @return void
	 */
	public function searchAction($searchPhrase) {
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
		$this->view->assign('settings', $this->settings);
		$args = $this->request->getArguments();
		$searchArg = $args['searchArg'];
		
		if($searchPhrase == '') {
			$searchArg = 0;
			$this->view->assign('searchArg', $searchArg);	
		}	
		if($searchArg == 1) {
			// Search by question	
			$searchFields = explode(',', $this->settings['fields']);
			$questions = $this->questionRepository->searchFor($searchFields, $searchPhrase);
			$this->view->assign('questions', $questions);	
			$this->view->assign('searchPhrase', $searchPhrase);	
			$this->view->assign('searchArg', $searchArg);	
		}
		if($searchArg == 2) {
			// Search by user
			$searchFields = explode(',', $this->settings['f_fields']);
			$users = $this->userRepository->searchFor($searchFields, $searchPhrase);
			$this->view->assign('users', $users);
			$this->view->assign('searchPhrase', $searchPhrase);
			$this->view->assign('searchArg', $searchArg);	
		}
	}
	
	/**
	 * search form, only form
	 *
	 * @ignorevalidation $searchPhrase
	 * @return void
	 */
	public function searchFormAction() {
		$this->view->assign('settings', $this->settings);
	}	


	/**
	 *
	 */
	protected function setTypeConverterConfigurationForImageUpload($argumentName) {
		$uploadConfiguration = array(
			UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
			UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
			UploadedFileReferenceConverter::CONFIGURATION_REPLACE_RESOURCE => TRUE,
		);
		/** @var PropertyMappingConfiguration $newImageConfiguration */
		$newImageConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
		$newImageConfiguration->forProperty('image')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceConverter',
				$uploadConfiguration
			);
		$newImageConfiguration->forProperty('imageCollection.0')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceConverter',
				$uploadConfiguration
			);
	}

}