<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
#use Devcompany\Devcompanycall\Property\TypeConverter\UploadedFileReferenceAnswerConverter;
#use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration; 

/**
 * AnswerController
 */
class AnswerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;	
	
	/**
	 * questionRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;		

	/**
	 * answerRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\AnswerRepository
	 * @inject
	 */
	protected $answerRepository = NULL;
	
	/**
	 * Access Control
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;
					

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$answers = $this->answerRepository->findAll();
		$this->view->assign('answers', $answers);
	}

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		$this->view->assign('answer', $answer);
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer
	 * @ignorevalidation $newAnswer
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer = NULL) {
		$this->view->assign('newAnswer', $newAnswer);
	}
	
	  /**
	 	* Set TypeConverter option for image upload
	 	*/
		public function initializeCreateAction() {
		//	$this->setTypeConverterConfigurationForImageAnswerUpload('newAnswer');
		}	

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question,
	\Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer) {
		if($this->accessControllService->isAccessAllowed($user)) {
			$this->view->assign('settings', $this->settings);
			$this->view->assign('user', $user);		
			$this->view->assign('question', $question);		
			$newAnswer->setUser($user);
			$newAnswer->setQuestion($question);		
			//$this->persistenceManager->persistAll();			
			$this->answerRepository->add($newAnswer);
			$this->redirect('last', 'Question');	
		} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last', 'Question');		
		}
	}

	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @ignorevalidation $answer
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
			$this->view->assign('user', $user);	
			$answerUser = $answer->getUser();
			$this->view->assign('answerUser', $answerUser);	
			if($user == $answerUser) {
				$this->view->assign('answer', $answer);		
			} else {
				$this->flashMessageContainer->add(
					'Вы не можете редактировать не ваш ответ',
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);				
			}
			
	}
		
	/**
	 * Set TypeConverter option for image upload
	 */
	public function initializeUpdateAction() {
		$this->setTypeConverterConfigurationForImageAnswerUpload('answer');
	}		

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
			if($this->accessControllService->isAccessAllowed($user)) {
				$this->view->assign('settings', $this->settings);
				$this->view->assign('user', $user);	
				$this->addFlashMessage('Ваш ответ сохранен', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
				$this->answerRepository->update($answer);
				$this->redirect('last', 'Question');	
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last', 'Question');				
			}

	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		$this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->answerRepository->remove($answer);
		$this->redirect('list');
	}


	/**
	 *
	 */
	protected function setTypeConverterConfigurationForImageAnswerUpload($argumentName) {
		$uploadConfiguration = array(
			UploadedFileReferenceAnswerConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
			UploadedFileReferenceAnswerConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
			UploadedFileReferenceAnswerConverter::CONFIGURATION_REPLACE_RESOURCE => TRUE,
		);
		/** @var PropertyMappingConfiguration $newImageAnswerConfiguration */
		$newImageAnswerConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
		$newImageAnswerConfiguration->forProperty('image')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceAnswerConverter',
				$uploadConfiguration
			);
		$newImageAnswerConfiguration->forProperty('imageCollection.0')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceAnswerConverter',
				$uploadConfiguration
			);
	}

}