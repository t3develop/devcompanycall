<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;	
	
	/**
	 * accessControll
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;		
	
	/**
	 * answerRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\AnswerRepository
	 * @inject
	 */
	protected $answerRepository = NULL;	
	
	/**
	 * questionRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;	

	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;
	
	/**
	 * messsageRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\MesssageRepository
	 * @inject
	 */
	protected $messsageRepository = NULL;	
	
	
		/**
 	 * 
 	 *
 	 * @return void
 	*/
	public function initializeAction() {
    	if ($this->arguments->hasArgument('user')) {
    		$propertyMappingConfiguration = $this->arguments['user']->getPropertyMappingConfiguration();
				$propertyMappingConfiguration->allowProperties('user');
						$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', 
						\TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
        
        //$this->arguments->getArgument('user')->getPropertyMappingConfiguration()->setTargetTypeForSubProperty('image', 'array');
    	}   	
	}	
	
	
	/**
	 * send email using swiftmailer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $addMessage
	 * @param string $recipientEmail
	 * @param string $recipientCc
	 * @param string $subject
	 * @param string $message
	 * @param string $senderEmail
	 * @param string $senderName
	 * @return void
	 */
	protected function sendMail($senderEmail, $recipientEmail, $subject, $message) {
		$this->view->assign('settings', $this->settings);
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		//$mail->setFrom($senderEmail);
		$mail->setFrom(array($this->settings['senderEmail']));
		$mail->setTo($recipientEmail);
		$mail->setSubject($this->settings['userEmailSubject']);
		//$mail->setCc($recipientCc);
		$mail->setBody(htmlspecialchars_decode($message), 'text/html');
		if($this->settings['debugMail'] == 1) {
			$this->debug($message);
		} else {
			$mail->send();
		}
	}	

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$users = $this->userRepository->findAll();
		$this->view->assign('users', $users);
	}
	
	/**
	 * action random list
	 *
	 * @return void
	 */
	public function randomListAction() {
		$users = $this->userRepository->findRandom();
		$this->view->assign('users', $users);
	}	

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage
	 * @ignorevalidation $newMesssage
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage = NULL) {
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
		//$args = $this->request->getArguments();
		//echo $args;
		$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('loggedUser', $loggedUser);	
		
		$this->view->assign('user', $user);	
		$answers = $this->answerRepository->findByUser($user);
		$this->view->assign('answers', $answers);		
		$this->view->assign('newMesssage', $newMesssage);
		
		$limit = 10;
		$this->view->assign('userQuestions', $this->questionRepository->findQuestionsByUser($user, $limit));
		$this->view->assign('userAnswers', $this->answerRepository->findAnswersByUser($user, $limit));	
	}
	
	
	/**
	 * action user list
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function userQuestionListAction($user) {
		$limit = 10000;
		$this->view->assign('user', $user);			
		$this->view->assign('userQuestions', $this->questionRepository->findQuestionsByUser($user, $limit));
	}	
	

	/**
	 * action user list
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function userAnswerListAction($user) {
		$limit = 10000;		
		$this->view->assign('user', $user);			
		$this->view->assign('userAnswers', $this->answerRepository->findAnswersByUser($user, $limit));	
	}		
	

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $newUser
	 * @ignorevalidation $newUser
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\User $newUser = NULL) {
		$this->view->assign('newUser', $newUser);
	}

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $newUser
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $newUser) {
		$this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->userRepository->add($newUser);
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @ignorevalidation $user
	 * @return void
	 */
	public function editAction() {
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);
	}

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->addFlashMessage('Ваша запись обновлена', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
		$this->userRepository->update($user);
		$this->redirect('settings', 'User');
	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\User $user) {
		$this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->userRepository->remove($user);
		$this->redirect('list');
	}
	
	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function profileAction() {
		//$this->addFlashMessage('Ваша запись успешно добавлена', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);		
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
		$answers = $this->answerRepository->findByUser($user);
		$this->view->assign('answers', $answers);						
		
		$limit = 10000;
		$this->view->assign('userQuestions', $this->questionRepository->findQuestionsByUser($user, $limit));
		$this->view->assign('userAnswers', $this->answerRepository->findAnswersByUser($user, $limit));	
	}
	

	/**
	 * action user rating
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function ratingAction() {	
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
	}	
	
	
	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function settingsAction() {
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
		$answers = $this->answerRepository->findByUser($user);
		$this->view->assign('answers', $answers);						
	}					
	
	
	/**
	 * action addMessage
	 * 
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage
	 * @dontverifyrequesthash
	 * @return void
	 */
	public function userEmailAction(\Devcompany\Devcompanycall\Domain\Model\User $user, 
		\Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage) {	
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);				
			
		$this->view->assign('settings', $this->settings);
		$this->view->assign('user', $user);		
		$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('loggedUser', $loggedUser);		
		$senderUserEmail = $loggedUser->getEmail();	
		$senderUserName = $loggedUser->getFirstName() .' '. $loggedUser->getLastName();
		
		//$this->messsageRepository->add($newMesssage);
		//$this->persistenceManager->persistAll();
		//$user->addMessage($newMessage);
		
		$message .= '<h3>Вам пришло сообщение от пользователя с сайта Обращайся.рф</h3>';
		$message .= '<b>Имя пользователя: </b>' . $senderUserName;
		$message .= '<br />';
		$message .= '<b>Email пользователя: </b>' . $senderUserEmail;
		$message .= '<br />';
		$message .= '<b>Сообщение пользователя: </b>' . $newMesssage->getMessage();
				
		// send email
		$this->sendMail(
				$senderEmail,
				$user->getEmail(),
				$this->settings['userEmailSubject'],
				$message
				//$newMesssage->getMessage()
		);	
		$this->addFlashMessage('Ваше сообщение отправлено', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
		$this->redirect('profile');
	}		

}