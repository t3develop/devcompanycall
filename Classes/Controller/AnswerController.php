<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
 
use Devcompany\Devcompanycall\Property\TypeConverter\UploadedFileReferenceConverter; 
//use Devcompany\Devcompanycall\Property\TypeConverter\UploadedFileReferenceAnswerConverter;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration; 

/**
 * AnswerController
 */
class AnswerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;	
	
	/**
	 * questionRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\QuestionRepository
	 * @inject
	 */
	protected $questionRepository = NULL;		

	/**
	 * answerRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\AnswerRepository
	 * @inject
	 */
	protected $answerRepository = NULL;
	
	/**
	 * Access Control
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;
	
	/**
	 * YouTubeUtility
	 *
	 * @var \Devcompany\Devcompanycall\Utility\YouTubeUtility
	 * @inject
	 */
	protected $youTubeUtility;	
	
	
	/**
	 * send email using swiftmailer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $addMessage
	 * @param string $recipientEmail
	 * @param string $recipientCc
	 * @param string $subject
	 * @param string $message
	 * @param string $senderEmail
	 * @param string $senderName
	 * @return void
	 */
	protected function sendMail($senderEmail, $recipientEmail, $subject, $message) {
		$this->view->assign('settings', $this->settings);
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		//$mail->setFrom($senderEmail);
		$mail->setFrom(array($this->settings['senderEmail']));
		$mail->setTo($recipientEmail);
		$mail->setSubject($this->settings['userEmailAnswerSubject']);
		//$mail->setCc($recipientCc);
		$mail->setBody(htmlspecialchars_decode($message), 'text/html');
		if($this->settings['debugMail'] == 1) {
			$this->debug($message);
		} else {
			$mail->send();
		}
	}		
		
		
	/**
	* @return void
	*/
/*
	protected function initializeCreateAction() {
 		$propertyMappingConfiguration = $this->arguments['newAnswer']->getPropertyMappingConfiguration();
 		$propertyMappingConfiguration->allowAllProperties();
		$propertyMappingConfiguration->allowProperties('point');
 		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}		
		*/			

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$answers = $this->answerRepository->findAll();
		$this->view->assign('answers', $answers);			
	}

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		$this->view->assign('answer', $answer);				
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer
	 * @ignorevalidation $newAnswer
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer = NULL) {
		$this->view->assign('newAnswer', $newAnswer);
	}
	
	
	/**
	* Set TypeConverter option for image upload
	*/
	public function initializeCreateAction() {
		$this->setTypeConverterConfigurationForImageAnswerUpload('newAnswer');
	}	
	
	
	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $question
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Question $question,
	\Devcompany\Devcompanycall\Domain\Model\Answer $newAnswer) {
		if($this->accessControllService->isAccessAllowed($user)) {
			//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_POST);
			$this->view->assign('settings', $this->settings);
			$this->view->assign('user', $user);		
			$this->view->assign('question', $question);		
			$newAnswer->setUser($user);
			$newAnswer->setQuestion($question);
			$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
 		
			$this->userRepository->setRateValue($user);		
			$this->persistenceManager->persistAll();
					
			$this->answerRepository->add($newAnswer);
			$question->addAnswer($newAnswer);
	
			$senderUserEmail = $loggedUser->getEmail();	
			$senderUserName = $loggedUser->getFirstName() .' '. $loggedUser->getLastName();
		
			$message .= '<h3>Вам пришел ответ на ваш вопрос от пользователя с сайта Обращайся.рф</h3>';
			$message .= '<b>Имя пользователя: </b>' . $senderUserName;
			$message .= '<br />';
			$message .= '<b>Сообщение пользователя: </b>' . htmlspecialchars($newAnswer->getAnswer());
			$message .= '<br />';
			$message .= '<b>Вы можете отслеживать ваши вопросы и ответы на странице: </b>';
			$message .= '<a href="http://обращайся.рф/profile/my-questions/">' .'http://обращайся.рф/profile/my-questions/' .'</a>';
			$message .= '<br />';					
			
			if($question->isNotice() == 1) {
				// send email
				$this->sendMail(
					$senderEmail,
					$question->getUser()->getEmail(),
					$this->settings['userEmailAnswerSubject'],
					$message
				);				
				//$this->redirect('edit', 'Answer');
				$this->redirect('last','Question',null,array(), '1');		
			}
				//$this->redirect('ok', 'Flash');
				$this->redirect('last','Question',null,array(), '1');	
			
		} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				//$this->redirect('error', 'Flash');	
				$this->redirect('last','Question',null,array(), '1');		
		}
	}


	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @ignorevalidation $answer
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		if($this->accessControllService->isAccessAllowed($user)) {
			$this->view->assign('user', $user);	
			$answerUser = $answer->getUser();
			$this->view->assign('answerUser', $answerUser);	
			if($user == $answerUser) {
				$this->view->assign('answer', $answer);		
			} else {
				$this->flashMessageContainer->add(
					'Вы не можете редактировать не ваш ответ',
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);				
			}			
		} else {
			$this->flashMessageContainer->add(
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   			\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
			);
			$this->redirect('error', 'Flash');				
		}			
	}
		
	/**
	 * Set TypeConverter option for image upload
	 */
	public function initializeUpdateAction() {
		$this->setTypeConverterConfigurationForImageAnswerUpload('answer');
	}		

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
		\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
			if($this->accessControllService->isAccessAllowed($user)) {
				$this->view->assign('settings', $this->settings);
				$this->view->assign('user', $user);	
				$this->addFlashMessage('Ваш ответ сохранен', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
				$this->answerRepository->update($answer);
				$this->redirect('last','Question',null,array(), '1');	
			} else {
				$this->flashMessageContainer->add(
     			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
					'',
   				\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
				);
				$this->redirect('last','Question',null,array(), '1');					
			}

	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Answer $answer
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Answer $answer) {
		if($this->accessControllService->isAccessAllowed($user)) {		
			$this->view->assign('settings', $this->settings);
			$this->view->assign('user', $user);				
			$this->addFlashMessage('Ваш вопрос удален', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
			
			$this->userRepository->deleteRateValue($user);
			$this->persistenceManager->persistAll();	
			$this->answerRepository->remove($answer);
			//$this->redirect('last', 'Question');	
			//$this->redirect('ok', 'Flash');			
		} else {
			$this->flashMessageContainer->add(
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
			'',
   			\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
			);
			//$this->redirect('last', 'Question');
			$this->redirect('last','Question',null,array(), '1');					
		}	

	}


	/**
	 *
	 */
	protected function setTypeConverterConfigurationForImageAnswerUpload($argumentName) {
		$uploadAnswerConfiguration = array(
			//UploadedFileReferenceAnswerConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
			//UploadedFileReferenceAnswerConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
			//UploadedFileReferenceAnswerConverter::CONFIGURATION_REPLACE_RESOURCE => TRUE,

			UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
			UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
			UploadedFileReferenceConverter::CONFIGURATION_REPLACE_RESOURCE => TRUE,
		);
		/** @var PropertyMappingConfiguration $newImageAnswerConfiguration */
		$newImageAnswerConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
		$newImageAnswerConfiguration->forProperty('image')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceAnswerConverter',
				$uploadAnswerConfiguration
			);
		$newImageAnswerConfiguration->forProperty('imageCollection.0')
			->setTypeConverterOptions(
				'Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceAnswerConverter',
				$uploadAnswerConfiguration
			);
	}

}