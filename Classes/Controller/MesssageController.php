<?php
namespace Devcompany\Devcompanycall\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * MesssageController
 */
class MesssageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;	
	
	/**
	 * accessControll
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;	
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;		

	/**
	 * messsageRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\MesssageRepository
	 * @inject
	 */
	protected $messsageRepository = NULL;
	

	/**
	 * send email using swiftmailer
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Question $addMessage
	 * @param string $recipientEmail
	 * @param string $recipientCc
	 * @param string $subject
	 * @param string $message
	 * @param string $senderEmail
	 * @param string $senderName
	 * @return void
	 */
	protected function sendMail($senderEmail, $recipientEmail, $subject, $message) {
		$this->view->assign('settings', $this->settings);
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		//$mail->setFrom($senderEmail);
		$mail->setFrom(array($this->settings['senderEmail']));
		$mail->setTo($recipientEmail);
		$mail->setSubject($this->settings['userEmailSubject']);
		//$mail->setCc($recipientCc);
		$mail->setBody(htmlspecialchars_decode($message), 'text/html');
		if($this->settings['debugMail'] == 1) {
			$this->debug($message);
		} else {
			$mail->send();
		}
	}	


	/**
	 * action list
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function listAction() {	
		$messsages = $this->messsageRepository->findAll();
		$this->view->assign('messsages', $messsages);
	}
	
	/**
	 * action blank
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function blankAction() {	
		$this->view->assign('settings', $this->settings);	
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
		$messsages = $this->messsageRepository->findByRecipient($user);
		$this->view->assign('messsages', $messsages);
	}	
	
	/**
	 * action inbox
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function inboxAction() {
		$this->view->assign('settings', $this->settings);	
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
		$messsages = $this->messsageRepository->findByRecipient($user);
		$this->view->assign('messsages', $messsages);
	}
	

	/**
	 * action inbox
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @return void
	 */
	public function outboxAction() {
		$this->view->assign('settings', $this->settings);	
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$this->view->assign('user', $user);	
		$messsages = $this->messsageRepository->findBySender($user);
		$this->view->assign('messsages', $messsages);
	}	
		

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage) {
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
		
		if($this->accessControllService->isAccessAllowed($user)) {
		$this->view->assign('settings', $this->settings);
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('user', $user);			
		$this->view->assign('messsage', $messsage);	
			
			$args = $this->request->getArguments();
			$argInbox = $args['inbox'];
			if($argInbox == 1) {
				$this->view->assign('inbox', 1);
			} else {
				$this->view->assign('inbox', 0);
			}			
				
		} else {
			$this->flashMessageContainer->add(
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   			\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
			);
			$this->redirect('last', 'Question');			
		}
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage
	 * @ignorevalidation $newMesssage
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage = NULL) {
		$this->view->assign('newMesssage', $newMesssage);
	}

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Messsage $newMesssage) {	
		$this->view->assign('settings', $this->settings);
		$this->view->assign('user', $user);		
		$loggedUser = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));		
		$newMesssage->setRecipient($user);	
		$newMesssage->setSender($loggedUser);		
		$this->persistenceManager->persistAll();	
				
		$senderUserEmail = $loggedUser->getEmail();	
		$senderUserName = $loggedUser->getFirstName() .' '. $loggedUser->getLastName();		
		
		$message .= '<h3>Вам пришло сообщение от пользователя с сайта Обращайся.рф</h3>';
		$message .= '<b>Имя пользователя: </b>' . $senderUserName;
		$message .= '<br />';
		//$message .= '<b>Email пользователя: </b>' . $senderUserEmail;
		//$message .= '<br />';
		$message .= '<b>Сообщение пользователя: </b>' . htmlspecialchars($newMesssage->getMessage());
		$message .= '<br />';
		$message .= '<b>Вы можете отслеживать ваши личные сообщения на странице: </b>';
		$message .= '<a href="http://обращайся.рф/profile/messages/">' .'http://обращайся.рф/profile/messages/' .'</a>';
		$message .= '<br />';
				
		// send email
		$this->sendMail(
				$senderEmail,
				$user->getEmail(),
				$this->settings['userEmailSubject'],
				$message
				//$newMesssage->getMessage()
		);		
		
		$this->addFlashMessage('Ваше сообщение отправлено', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->messsageRepository->add($newMesssage);
		//$this->redirect('flash', 'Messsage');	
		$this->redirect('outbox');
		
	}


	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @ignorevalidation $messsage
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage) {
		$this->view->assign('messsage', $messsage);
	}

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->messsageRepository->update($messsage);
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage) {
		if($this->accessControllService->isAccessAllowed($user)) {
			//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
			$this->addFlashMessage('Сообщение успешно удалено', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
			$this->messsageRepository->remove($messsage);
			//$this->redirect('ok', 'Flash');			
			$this->redirect('inbox','Messsage',null,array(), '64');
		} else {
			$this->flashMessageContainer->add(
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   			\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
			);
			$this->redirect('error', 'Flash');			
		}
	}

}