<?php
namespace Devcompany\Devcompanycall\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ReplyController
 */
class ReplyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * persistenceManager
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;	
	
	/**
	 * configurationManager
	 *
	 * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;	
	
	/**
	 * accessControll
	 *
	 * @var \Devcompany\Devcompanycall\Service\AccessControlService
	 * @inject
	 */
	protected $accessControllService;	
	
	/**
	 * userRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\UserRepository
	 * @inject
	 */
	protected $userRepository = NULL;		

	/**
	 * replyRepository
	 *
	 * @var \Devcompany\Devcompanycall\Domain\Repository\ReplyRepository
	 * @inject
	 */
	protected $replyRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$replies = $this->replyRepository->findAll();
		$this->view->assign('replies', $replies);
	}

	/**
	 * action show
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $reply
	 * @return void
	 */
	public function showAction(\Devcompany\Devcompanycall\Domain\Model\Reply $reply) {
		$this->view->assign('reply', $reply);
	}

	/**
	 * action new
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $newReply
	 * @ignorevalidation $newReply
	 * @return void
	 */
	public function newAction(\Devcompany\Devcompanycall\Domain\Model\Reply $newReply = NULL) {
		$this->view->assign('newReply', $newReply);
	}

	/**
	 * action create
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Messsage $messsage
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $newReply
	 * @return void
	 */
	public function createAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Messsage $messsage,
	\Devcompany\Devcompanycall\Domain\Model\Reply $newReply) {
		$this->view->assign('settings', $this->settings);
		$user = $this->userRepository->findByUid(intval($GLOBALS['TSFE']->fe_user->user['uid']));	
		$this->view->assign('user', $user);	
		$newReply->setUser($user);	
		$newReply->setMesssage($messsage);			
		$this->addFlashMessage('Ответ создан', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
		$this->replyRepository->add($newReply);
		//$this->redirect('profile', 'User');
		//$this->redirect('ok', 'Flash');	
		$this->redirect('inbox','Messsage',null,array(), '64');
	}

	/**
	 * action edit
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $reply
	 * @ignorevalidation $reply
	 * @return void
	 */
	public function editAction(\Devcompany\Devcompanycall\Domain\Model\Reply $reply) {
		$this->view->assign('reply', $reply);
	}

	/**
	 * action update
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $reply
	 * @return void
	 */
	public function updateAction(\Devcompany\Devcompanycall\Domain\Model\Reply $reply) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->replyRepository->update($reply);
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Devcompany\Devcompanycall\Domain\Model\User $user
	 * @param \Devcompany\Devcompanycall\Domain\Model\Reply $reply
	 * @return void
	 */
	public function deleteAction(\Devcompany\Devcompanycall\Domain\Model\User $user,
	\Devcompany\Devcompanycall\Domain\Model\Reply $reply) {
		//\TYPO3\CMS\Core\Utility\DebugUtility::debug($_REQUEST);
		if($this->accessControllService->isAccessAllowed($user)) {
			$this->view->assign('settings', $this->settings);
			$this->view->assign('user', $user);	
			
			$this->addFlashMessage('Сообщение успешно удалено', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
			$this->replyRepository->remove($reply);
			//$this->redirect('list');
			$this->redirect('inbox','Messsage',null,array(), '64');		
		} else {
			$this->flashMessageContainer->add(
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
     		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_devcompanycall.youMustBeLogin', 'Devcompanycall'),
   			\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
		);
			$this->redirect('last','Question',null,array(), '1');			
		}		
	}

}