<?php
namespace Devcompany\Devcompanycall\ViewHelpers;
 
class CurrentDateViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     *
     * @param string $currentdate
     * @return string
     */
    public function render($currentdate) {
    //$currentdate = date ("m.d.Y");
    //$currentdate = strtotime('midnight');
    $fiveMin = 3*60+1; // 5 Min, 1 sek.
    //$currentdate = time()-$fuenfMin;
    $currentdate = date('m/d/Y h:i:s a', time()-$fiveMin);
		return $currentdate;
    }
}
?>