<?php
namespace Devcompany\Devcompanycall\ViewHelpers;
 
class EditDateViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
    /**
     *
     * @param string $currentdate
     * @return string
     */
    public function render($editdate) {
    $oneHour = 120*60+1; // 3 Min, 1 sek.
    $editdate = date('m/d/Y h:i:s a', time()-$oneHour);
    return $editdate;
    }
}
?>