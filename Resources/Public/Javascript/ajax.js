(function($) {
	$(function() {
		$('#ajax-submit').on('click', function(event) {
			event.preventDefault();
			var $nameField = $('#greeted-name');
			if (!$nameField.val()) {
				alert("Please enter a name!");
				return;
			}
			var $submitButton = $(this);
			var uri = $submitButton.data('ajaxuri');
			var parameters = {};
			parameters[$nameField.attr("name")] = $nameField.val();
			$.ajax(
				uri,
				{
					"type": "post",
					"data": parameters
				}
			).done(function(result) {
				$(".tx-ajax-example").html(result);
			});
		});


	});
})(jQuery);


$(document).ready(function() {
  $('.ajax-link').click(function() {
    if (confirm('Вы уверены, что хотите удалить запись?')) {


		function registerAjaxInterceptor(linkElementSelector, resultElementSelector, outputTime) {
			outputTime = outputTime || false;
			$('body').on('click', linkElementSelector, function(event) {
				event.preventDefault();
				var $linkElement = $(this);
				var uri = $linkElement.attr('href');
				var startTime = new Date().getTime();
				var duration = 0;

				$.ajax(
					uri,
					{
						"type": "post"
					}
				).done(function(result) {
					duration = new Date().getTime() - startTime;
					//if (outputTime) {
					//	result = duration + 'ms <br>' + result;
					//}
					$(".inbox-list").css("display", "none");
					$(resultElementSelector).html(result); 	
					//alert('Сообщение удалено');		
				});
			});
		}
		registerAjaxInterceptor('.ajax-link', '#ajax-results', true);
    } else {

		function registerAjaxInterceptor2(linkElementSelector, resultElementSelector, outputTime) {
			outputTime = outputTime || false;
			$('body').on('click', linkElementSelector, function(event) {
				event.preventDefault();
				var $linkElement = $(this);
				//var uri = $linkElement.attr('href');
				//var startTime = new Date().getTime();
				var duration = 0;

				$.ajax(
					uri,
					{
						//"type": "post"
					}
				).done(function(result) {
					$(".inbox-list").css("display", "none");
					$(resultElementSelector).html(result); 					
				});
			});
		}
		registerAjaxInterceptor2('.ajax-link', '#ajax-results', false);


    }
  });
});

