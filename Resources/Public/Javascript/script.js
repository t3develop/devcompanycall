jQuery(document).ready(function(){
  jQuery("#form, #appeal, #search").validationEngine();
  jQuery("abbr.timeago").timeago();  
});

//$(document).ready(function(){
	//prepareDynamicDates();
//});

bkLib.onDomLoaded(function() {
	//new nicEditor().panelInstance('description');
	//new nicEditor ({buttonList : ['fontSize','bold','italic','underline','strikeThrough','html','image','ul','ol']}).panelInstance('description');
	new nicEditor ({buttonList : ['fontSize','bold','italic','underline','strikeThrough','html','link','ul','ol']}).panelInstance('description');
});
