<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'Devcompanycall','Devcompany Call');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallCategory','Devcompany Call Category');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallRandomUsers','Devcompany Call Random Users');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallAskButton','Devcompany Ask Button');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallSearch','Devcompany Search');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallUserRating','Devcompany User Rating');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'DevcompanycallNotification','Devcompany Notification');

// FlexForm
$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . devcompanycall;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' .devcompanycall. '.xml');
/// FlexForm

// FlexForm 2
$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . devcompanycallsearch;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' .devcompanycallsearch. '.xml');
/// FlexForm 2

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Devcompany Call');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_question', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_question.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_question');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_question'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,description,create_date,category,answer,image,user,notice,count,private,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Question.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_question.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_category', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_category.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_category');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_category'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_category',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,description,parentcategory,question,user,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Category.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_category.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_answer', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_answer.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_answer');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_answer'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_answer',
		'label' => 'answer',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'answer,create_date,user,question,image,reply,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Answer.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_answer.gif'
	),
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_reply', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_reply.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_reply');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_reply'] = array(
	'ctrl' => array(
		'title'	=> 'Reply',
		'label' => 'reply',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'reply,user,create_date,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Reply.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_reply.gif'
	),
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_messsage', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_messsage.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_messsage');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_messsage'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_messsage',
		'label' => 'message',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'subject,message,create_date,user,sender,recipient,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Messsage.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_messsage.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_appeal', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_appeal.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_appeal');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_appeal'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_appeal',
		'label' => 'message',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'subject,message,create_date,user,sender,question,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Appeal.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_appeal.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_wall', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_wall.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_wall');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_wall'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_wall',
		'label' => 'subject',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'subject,message,create_date,image,sender,recipient,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Wall.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_wall.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_devcompanycall_domain_model_filereference', 'EXT:devcompanycall/Resources/Private/Language/locallang_csh_tx_devcompanycall_domain_model_filereference.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_filereference');
$GLOBALS['TCA']['tx_devcompanycall_domain_model_filereference'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_filereference',
		'label' => 'uid',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => '',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/FileReference.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_devcompanycall_domain_model_filereference.gif'
	),
);

/*
if (!isset($GLOBALS['TCA']['fe_users']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumns = array();
	$tempColumns[$GLOBALS['TCA']['fe_users']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(),
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumns, 1);
}
*/

t3lib_div::loadTCA('fe_users');
$tmp_devcompanycall_columns = array(

	'username' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.username',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'email' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.email',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'first_name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.first_name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'last_name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.last_name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'lastlogin' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.lastlogin',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'is_online' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.is_online',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),	
	'crdate' => array(
		'exclude' => 1,
		'label' => 'Create Date',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	
	/*
	'image' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.image',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	*/
	'question' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.question',
		'config' => array(
			'type' => 'inline',
			'foreign_table' => 'tx_devcompanycall_domain_model_question',
			'foreign_field' => 'user',
			'foreign_default_sortby' => 'ORDER BY create_date,sorting ASC',
			'foreign_sortby' => 'create_date',
			'maxitems'      => 9999,
			'appearance' => array(
				'collapseAll' => 1,
				'levelLinksPosition' => 'top',
				'showSynchronizationLink' => 1,
				'showPossibleLocalizationRecords' => 1,
				'useSortable' => 1,
				'showAllLocalizationLink' => 1
			),
		),

	),
	'message' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.message',
		'config' => array(
			'type' => 'inline',
			'foreign_table' => 'tx_devcompanycall_domain_model_messsage',
			'foreign_field' => 'user',
			'maxitems'      => 9999,
			'appearance' => array(
				'collapseAll' => 1,
				'levelLinksPosition' => 'top',
				'showSynchronizationLink' => 1,
				'showPossibleLocalizationRecords' => 1,
				'showAllLocalizationLink' => 1
			),
		),

	),
	'comments' => array(
		'exclude' => 1,
		'label' => 'Настроение',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 5,
			'eval' => 'trim'
		)
	),	
	/*
	'point' => array(
		'exclude' => 1,
		'label' => 'Баллы',
		'config' => array(
			'type' => 'input',
			'size' => 4,
			'eval' => 'trim',
			//'eval' => 'int',
			//'readOnly' => 1,
		)
	),
	*/	
	
	'rate' => array(
		'exclude' => 1,
		'label' => 'Рейтинг',
		'config' => array(
			'type' => 'input',
			'size' => 4,
			'eval' => 'int'
		)
	),	
	'category' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user.category',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_devcompanycall_domain_model_category',
			'MM' => 'tx_devcompanycall_user_category_mm',
			'size' => 10,
			'autoSizeMax' => 30,
			'maxitems' => 5,
			'multiple' => 0,
			'wizards' => array(
				'_PADDING' => 1,
				'_VERTICAL' => 1,
				'edit' => array(
					'type' => 'popup',
					'title' => 'Edit',
					'script' => 'wizard_edit.php',
					'icon' => 'edit2.gif',
					'popup_onlyOpenIfSelected' => 1,
					'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
				'add' => Array(
					'type' => 'script',
					'title' => 'Create new',
					'icon' => 'add.gif',
					'params' => array(
						'table' => 'tx_devcompanycall_domain_model_category',
						'pid' => '###CURRENT_PID###',
						'setValue' => 'prepend'
						),
					'script' => 'wizard_add.php',
				),
			),
		),
	),	
	
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_devcompanycall_columns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCATypes('fe_users','gender','', 'after:name');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCATypes(
	'fe_users',
	'--div--;LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user,rate,username,email,question,category;;;;1-1-1'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_question');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_devcompanycall_domain_model_messsage');

/*
$GLOBALS['TCA']['fe_users']['types']['Tx_Devcompanycall_User']['showitem'] = $TCA['fe_users']['types']['0']['showitem'];
$GLOBALS['TCA']['fe_users']['types']['Tx_Devcompanycall_User']['showitem'] .= ',--div--;LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_user,';
$GLOBALS['TCA']['fe_users']['types']['Tx_Devcompanycall_User']['showitem'] .= 'username, email, first_name, last_name, lastlogin, is_online, image, question, message';

$GLOBALS['TCA']['fe_users']['columns'][$TCA['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_Devcompanycall_User','Tx_Devcompanycall_User');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', $GLOBALS['TCA']['fe_users']['ctrl']['type'],'','after:' . $TCA['fe_users']['ctrl']['label']);
*/