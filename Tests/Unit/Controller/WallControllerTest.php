<?php
namespace Devcompany\Devcompanycall\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Devcompany\Devcompanycall\Controller\WallController.
 *
 * @author Dmitry Vasiliev <dmitry@typo3.ru.net>
 */
class WallControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Devcompany\Devcompanycall\Controller\WallController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Devcompany\\Devcompanycall\\Controller\\WallController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllWallsFromRepositoryAndAssignsThemToView() {

		$allWalls = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$wallRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\WallRepository', array('findAll'), array(), '', FALSE);
		$wallRepository->expects($this->once())->method('findAll')->will($this->returnValue($allWalls));
		$this->inject($this->subject, 'wallRepository', $wallRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('walls', $allWalls);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenWallToView() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('wall', $wall);

		$this->subject->showAction($wall);
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenWallToView() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newWall', $wall);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($wall);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenWallToWallRepository() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$wallRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\WallRepository', array('add'), array(), '', FALSE);
		$wallRepository->expects($this->once())->method('add')->with($wall);
		$this->inject($this->subject, 'wallRepository', $wallRepository);

		$this->subject->createAction($wall);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenWallToView() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('wall', $wall);

		$this->subject->editAction($wall);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenWallInWallRepository() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$wallRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\WallRepository', array('update'), array(), '', FALSE);
		$wallRepository->expects($this->once())->method('update')->with($wall);
		$this->inject($this->subject, 'wallRepository', $wallRepository);

		$this->subject->updateAction($wall);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenWallFromWallRepository() {
		$wall = new \Devcompany\Devcompanycall\Domain\Model\Wall();

		$wallRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\WallRepository', array('remove'), array(), '', FALSE);
		$wallRepository->expects($this->once())->method('remove')->with($wall);
		$this->inject($this->subject, 'wallRepository', $wallRepository);

		$this->subject->deleteAction($wall);
	}
}
