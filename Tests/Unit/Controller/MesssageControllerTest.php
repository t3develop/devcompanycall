<?php
namespace Devcompany\Devcompanycall\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Devcompany\Devcompanycall\Controller\MesssageController.
 *
 * @author Dmitry Vasiliev <dmitry@typo3.ru.net>
 */
class MesssageControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Devcompany\Devcompanycall\Controller\MesssageController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Devcompany\\Devcompanycall\\Controller\\MesssageController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllMesssagesFromRepositoryAndAssignsThemToView() {

		$allMesssages = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$messsageRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\MesssageRepository', array('findAll'), array(), '', FALSE);
		$messsageRepository->expects($this->once())->method('findAll')->will($this->returnValue($allMesssages));
		$this->inject($this->subject, 'messsageRepository', $messsageRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('messsages', $allMesssages);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenMesssageToView() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('messsage', $messsage);

		$this->subject->showAction($messsage);
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenMesssageToView() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newMesssage', $messsage);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($messsage);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenMesssageToMesssageRepository() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$messsageRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\MesssageRepository', array('add'), array(), '', FALSE);
		$messsageRepository->expects($this->once())->method('add')->with($messsage);
		$this->inject($this->subject, 'messsageRepository', $messsageRepository);

		$this->subject->createAction($messsage);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenMesssageToView() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('messsage', $messsage);

		$this->subject->editAction($messsage);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenMesssageInMesssageRepository() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$messsageRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\MesssageRepository', array('update'), array(), '', FALSE);
		$messsageRepository->expects($this->once())->method('update')->with($messsage);
		$this->inject($this->subject, 'messsageRepository', $messsageRepository);

		$this->subject->updateAction($messsage);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenMesssageFromMesssageRepository() {
		$messsage = new \Devcompany\Devcompanycall\Domain\Model\Messsage();

		$messsageRepository = $this->getMock('Devcompany\\Devcompanycall\\Domain\\Repository\\MesssageRepository', array('remove'), array(), '', FALSE);
		$messsageRepository->expects($this->once())->method('remove')->with($messsage);
		$this->inject($this->subject, 'messsageRepository', $messsageRepository);

		$this->subject->deleteAction($messsage);
	}
}
