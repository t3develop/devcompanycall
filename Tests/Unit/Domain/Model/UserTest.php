<?php

namespace Devcompany\Devcompanycall\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dmitry Vasiliev <dmitry@typo3.ru.net>, Devcompany
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Devcompany\Devcompanycall\Domain\Model\User.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Dmitry Vasiliev <dmitry@typo3.ru.net>
 */
class UserTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Devcompany\Devcompanycall\Domain\Model\User
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \Devcompany\Devcompanycall\Domain\Model\User();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getUsernameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getUsername()
		);
	}

	/**
	 * @test
	 */
	public function setUsernameForStringSetsUsername() {
		$this->subject->setUsername('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'username',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEmailReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getEmail()
		);
	}

	/**
	 * @test
	 */
	public function setEmailForStringSetsEmail() {
		$this->subject->setEmail('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'email',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getFirstNameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getFirstName()
		);
	}

	/**
	 * @test
	 */
	public function setFirstNameForStringSetsFirstName() {
		$this->subject->setFirstName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'firstName',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLastNameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getLastName()
		);
	}

	/**
	 * @test
	 */
	public function setLastNameForStringSetsLastName() {
		$this->subject->setLastName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'lastName',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLastloginReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getLastlogin()
		);
	}

	/**
	 * @test
	 */
	public function setLastloginForStringSetsLastlogin() {
		$this->subject->setLastlogin('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'lastlogin',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIsOnlineReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getIsOnline()
		);
	}

	/**
	 * @test
	 */
	public function setIsOnlineForStringSetsIsOnline() {
		$this->subject->setIsOnline('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'isOnline',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getImage()
		);
	}

	/**
	 * @test
	 */
	public function setImageForStringSetsImage() {
		$this->subject->setImage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'image',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getQuestionReturnsInitialValueForQuestion() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getQuestion()
		);
	}

	/**
	 * @test
	 */
	public function setQuestionForObjectStorageContainingQuestionSetsQuestion() {
		$question = new \Devcompany\Devcompanycall\Domain\Model\Question();
		$objectStorageHoldingExactlyOneQuestion = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneQuestion->attach($question);
		$this->subject->setQuestion($objectStorageHoldingExactlyOneQuestion);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneQuestion,
			'question',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addQuestionToObjectStorageHoldingQuestion() {
		$question = new \Devcompany\Devcompanycall\Domain\Model\Question();
		$questionObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$questionObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($question));
		$this->inject($this->subject, 'question', $questionObjectStorageMock);

		$this->subject->addQuestion($question);
	}

	/**
	 * @test
	 */
	public function removeQuestionFromObjectStorageHoldingQuestion() {
		$question = new \Devcompany\Devcompanycall\Domain\Model\Question();
		$questionObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$questionObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($question));
		$this->inject($this->subject, 'question', $questionObjectStorageMock);

		$this->subject->removeQuestion($question);

	}

	/**
	 * @test
	 */
	public function getMessageReturnsInitialValueForMesssage() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getMessage()
		);
	}

	/**
	 * @test
	 */
	public function setMessageForObjectStorageContainingMesssageSetsMessage() {
		$message = new \Devcompany\Devcompanycall\Domain\Model\Messsage();
		$objectStorageHoldingExactlyOneMessage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneMessage->attach($message);
		$this->subject->setMessage($objectStorageHoldingExactlyOneMessage);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneMessage,
			'message',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addMessageToObjectStorageHoldingMessage() {
		$message = new \Devcompany\Devcompanycall\Domain\Model\Messsage();
		$messageObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$messageObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($message));
		$this->inject($this->subject, 'message', $messageObjectStorageMock);

		$this->subject->addMessage($message);
	}

	/**
	 * @test
	 */
	public function removeMessageFromObjectStorageHoldingMessage() {
		$message = new \Devcompany\Devcompanycall\Domain\Model\Messsage();
		$messageObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$messageObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($message));
		$this->inject($this->subject, 'message', $messageObjectStorageMock);

		$this->subject->removeMessage($message);

	}
}
