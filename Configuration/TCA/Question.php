<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_devcompanycall_domain_model_question'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_devcompanycall_domain_model_question']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, video_id, create_date, category, answer, image, user, notice, count, sender, recipient, private',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, notice, description, video_id, create_date, category, user, sender, recipient, private, count, 
		--div--; Ответы, answer, 
		--div--; Изображение, image, 
		--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_devcompanycall_domain_model_question',
				'foreign_table_where' => 'AND tx_devcompanycall_domain_model_question.pid=###CURRENT_PID### AND tx_devcompanycall_domain_model_question.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),	
		'video_id' => array(
			'exclude' => 1,
			'label' => 'Video',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),	
		
		'create_date' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.create_date',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'eval' => 'datetime',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'category' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.category',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_devcompanycall_domain_model_category',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'answer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.answer',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_devcompanycall_domain_model_answer',
				'foreign_field' => 'question',
				'foreign_sortby' => 'create_date',
				//'foreign_default_sortby' => 'ORDER BY crdate,sorting ASC',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		/*
		'image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.image',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_devcompanycall_domain_model_filereference',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		*/
		
		'notice' => array(
			'exclude' => 1,
			'label' => 'Уведомление на ответ по email',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),			
		
		'image' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', array(
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
				),
				'maxitems' => 1,
				// custom configuration for displaying fields in the overlay/reference table
				// to use the imageoverlayPalette instead of the basicoverlayPalette
				'foreign_match_fields' => array(
					'fieldname' => 'image',
					'tablenames' => 'tx_devcompanycall_domain_model_question',
					'table_local' => 'sys_file',
				),
				'foreign_types' => array(
					'0' => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					)
				)
			), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])
		),		
		
		'image_collection' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:upload_example/Resources/Private/Language/locallang_db.xlf:tx_uploadexample_domain_model_example.image_collection',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image_collection', array(
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
				),
				// custom configuration for displaying fields in the overlay/reference table
				// to use the imageoverlayPalette instead of the basicoverlayPalette
				'foreign_match_fields' => array(
					'fieldname' => 'image_collection',
					'tablenames' => 'tx_devcompanycall_domain_model_question',
					'table_local' => 'sys_file',
				),
				'foreign_types' => array(
					'0' => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					)
				)
			), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])
		),			
		'count' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.count',
			'config' => array(
				'readOnly' => 'true',
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),	
					
		'user' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.user',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'fe_users',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'sender' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_messsage.sender',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'fe_users',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'recipient' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_messsage.recipient',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'fe_users',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'private' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:devcompanycall/Resources/Private/Language/locallang_db.xlf:tx_devcompanycall_domain_model_question.private',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		
		/*'user' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),*/
		
		/*
		'category' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		*/
	),
);
