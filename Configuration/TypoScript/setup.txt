
plugin.tx_devcompanycall {
  settings {
    domain = {$domain}
    detailPageUid = {$plugin.tx_devcompanycall.settings.detailPageUid}
    detailUserPageUid = {$plugin.tx_devcompanycall.settings.detailUserPageUid}
    editQuestionPageUid = {$plugin.tx_devcompanycall.settings.editQuestionPageUid} 
    detailMessageInboxPageUid = {$plugin.tx_devcompanycall.settings.detailMessageInboxPageUid}  
    
    adminEmail = {$plugin.tx_devcompanycall.settings.adminEmail} 
    senderEmail = {$plugin.tx_devcompanycall.settings.senderEmail} 
    appealEmailSubject = {$plugin.tx_devcompanycall.settings.appealEmailSubject} 
    userEmailSubject = {$plugin.tx_devcompanycall.settings.userEmailSubject} 
    userEmailAnswerSubject = {$plugin.tx_devcompanycall.settings.userEmailAnswerSubject} 
    userEmailQuestionSubject = {$plugin.tx_devcompanycall.settings.userEmailQuestionSubject}   
    
    editAnswerPageUid = {$plugin.tx_devcompanycall.settings.editAnswerPageUid}
    fields = {$plugin.tx_devcompanycall.settings.searchFields}
    f_fields = {$plugin.tx_devcompanycall.settings.searchUserFields}
    searchPageUid = {$plugin.tx_devcompanycall.settings.searchPageUid}
    
    ajaxPageType = {$plugin.tx_devcompanycall.settings.ajaxPageType}
    
    searchRequestBlank = {$plugin.tx_devcompanycall.settings.searchRequestBlank}
    searchRequestByQuestion = {$plugin.tx_devcompanycall.settings.searchRequestByQuestion}
    searchRequestByUser = {$plugin.tx_devcompanycall.settings.searchRequestByUser}
    
    pointValue = {$plugin.tx_devcompanycall.settings.pointValue}
    deletedPointValue = {$plugin.tx_devcompanycall.settings.deletedPointValue}
    list.paginate.itemsPerPage = {$plugin.tx_devcompanycall.settings.list.paginate.itemsPerPage}
    
    // Item Limit
    //limit = {$plugin.tx_devcompanycall.settings.limit}
    
    list {
      rss {
        channel {
          title = {$plugin.tx_devcompanycall.rss.channel.title}
          description = {$plugin.tx_devcompanycall.rss.channel.description}
          language = {$plugin.tx_devcompanycall.rss.channel.language}
          copyright = {$plugin.tx_devcompanycall.rss.channel.copyright}
          generator = {$plugin.tx_devcompanycall.rss.channel.generator}
          link = {$plugin.tx_devcompanycall.rss.channel.link}
        }
      }
    }

         
  }
  
	view {
		templateRootPath = {$plugin.tx_devcompanycall.view.templateRootPath}
		partialRootPath = {$plugin.tx_devcompanycall.view.partialRootPath}
		layoutRootPath = {$plugin.tx_devcompanycall.view.layoutRootPath}
		widget {
		  #TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = EXT:pfad/zum/Ordner/der/Templates/
		  TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = EXT:devcompanycall/Resources/Private/Templates/
		}
	}
	persistence {
		storagePid = {$plugin.tx_devcompanycall.persistence.storagePid}
		
    #enableAutomaticCacheClearing = 1
    #updateReferenceIndex = 0
    
    classes {
        Devcompany\Devcompanycall\Domain\Model\Answer {
        mapping {
            #tableName = fe_users
            columns {
                image.mapOnProperty = image
                first_name.mapOnProperty = firstName
                }
            }
        }
      }
		
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
}

plugin.tx_devcompanycall._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-devcompanycall table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-devcompanycall table th {
		font-weight:bold;
	}

	.tx-devcompanycall table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)

page.includeCSS {
  css_1_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/style.css
  css_10_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/validationEngine.jquery.css
  css_20_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/simplePagination.css
  css_30_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/previewForm.css
  css_40_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/jquery.datetimepicker.css
  #css_50_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/jquery.confirm.css
  css_50_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Css/video-url-replace.css
}

page.includeJS {
  js_10_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jquery.validationEngine.js
  js_20_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jquery.validationEngine-ru.js
  
  js_30_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jquery.timeago.js
  js_40_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jquery.timeago.ru.js
  
  js_50_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jPaginate.js
  #js_50_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/pagination.js
  
  js_60_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/previewForm.js
  js_70_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/jquery.datetimepicker.js
  js_80_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/nicEdit.js
  #js_90_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/ajax.js
  js_100_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/video-url-replace.js
  
  js_999_tx_devcompanycall = EXT:devcompanycall/Resources/Public/Javascript/script.js
}
