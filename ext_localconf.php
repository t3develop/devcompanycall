<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'Devcompanycall',
	array(
		'User' => 'list, show, new, create, edit, update, delete',
		'Question' => 'list, show, new, create, edit, update, delete, nonAnswerList',
		'Category' => 'list, show, new, create, edit, update, delete',
		'Answer' => 'list, show, new, create, edit, update, delete',
		'Messsage' => 'list, show, new, create, edit, update, delete, blank',
		'Wall' => 'list, show, new, create, edit, update, delete',
		'Flash' => 'ok, error',
		'Reply' => 'create',
		'Appeal' => 'list, show, new, create, edit, update, delete',
		
	),
	// non-cacheable actions
	array(
		'User' => 'create, update, delete',
		'Question' => 'create, update, delete',
		'Category' => 'create, update, delete',
		'Answer' => 'create, update, delete',
		'Messsage' => 'create, update, delete',
		'Wall' => 'create, update, delete',
		'Flash' => '',
		'Reply' => 'create',
		'Appeal' => 'show, create, update, delete',		
	)
	
		/*
	array(
		'User' => 'create, update, delete, profile, settings, list, show',
		'Question' => 'create, update, delete, edit',
		'Category' => 'create, update, delete',
		'Answer' => 'edit, list, show, create, update, delete',
		'Messsage' => 'create, update, delete, inbox, outbox, show, blank',
		'Wall' => 'create, update, delete',
		'Flash' => 'ok, error',
		'Reply' => 'create',
		'Appeal' => 'show, create, edit, update, delete',		
	)
	*/
);

/**
 * register cache for extension
 */
if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['devcompanycall_cache'])) {
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['devcompanycall_cache'] = array();
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['frontend'] = 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['devcompanycall_cache']['options']['compression'] = 1;
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceConverter');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Devcompany\\Devcompanycall\\Property\\TypeConverter\\UploadedFileReferenceAnswerConverter');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Devcompany\\Devcompanycall\\Property\\TypeConverter\\ObjectStorageConverter');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallCategory',
	array(
		'Category' => 'list',	
	),
	// non-cacheable actions
	array(
		'Category' => '',	
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallRandomUsers',
	array(
		'User' => 'randomList',	
	),
	// non-cacheable actions
	array(
		'User' => '',		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallAskButton',
	array(
		'Question' => 'askButton',
	),
	// non-cacheable actions
	array(
		'Question' => '',	
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallSearch',
	array(
		'Question' => 'searchForm, search',	
	),
	// non-cacheable actions
	array(
		'Question' => '',			
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallUserRating',
	array(
		'User' => 'rating',	
	),
	// non-cacheable actions
	array(
		'User' => 'rating',			
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Devcompany.' . $_EXTKEY,
	'DevcompanycallNotification',
	array(
		'User' => 'notification',	
	),
	// non-cacheable actions
	array(
		'User' => '',			
	)
);